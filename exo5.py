import pandas as pd
import numpy as np

data = pd.read_csv("./flights.csv", index_col=0)

result = data.loc[(data['year'] == 2013) & (data['month'] == 12) & (data['day'] == 24), ['hour', 'dep_delay']]
print(result)

retards = data[['dest', 'dep_delay']]
print("1-----------")
print(retards.head())
retards = retards[retards['dep_delay'] > 0]
print("2-----------")
print(retards.head())
retards = retards.groupby(by='origin').mean()
print("3-----------")
print(retards.head())
