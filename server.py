from flask import Flask, request
from flask_cors import CORS
from .backend import MyRenderer

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

my_renderer = MyRenderer()


@app.route('/')
def hello():
    return 'hello world'


@app.route('/predict', methods=['POST'])
def predict():
    return my_renderer.get_api()


@app.route('/add_data', methods=['POST'])
def add_data():
    print(request.get_json())