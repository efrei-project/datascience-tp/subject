import numpy as np

matrice = np.asarray([
    [1, 2, 3, 4],
    [2, 2, 3, 4],
    [3, 3, 3, 4],
    [4, 4, 4, 4]
])
print("Taille de la matrice :", matrice.shape) # La fonction 'shape' donne la forme (ou taille) de la matrice
matrice
