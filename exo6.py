import pandas as pd
import numpy as np

data = pd.read_csv("./flights.csv", index_col=0)

a = data.loc[data['origin'] == 'JFK', ['month', 'arr_delay']]
b = a[a['arr_delay'] > 0]
c = b.groupby(by='month').mean().sort_values(by='arr_delay', ascending=False)

print(c)

from_jfk = data.loc[data['origin'] == 'JFK']
only_few_column = from_jfk[['dest', 'month', 'dep_delay']]
result = only_few_column.groupby(by='month').mean().sort_values(by='dep_delay', ascending=False)

