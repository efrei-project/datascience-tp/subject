import pandas as pd
import matplotlib.pyplot as plt

student = pd.read_csv(r'../datasource/student-alcohol-consumption/student-por.csv', encoding="ISO-8859-1")

datasource = pd.DataFrame(student[['Mjob', 'Fjob', 'G3']])
dummies = pd.get_dummies(student[['Mjob', 'Fjob']])

to_display = datasource.groupby(['Mjob', 'Fjob']).mean()
to_display['median'] = datasource.groupby(['Mjob', 'Fjob']).median()

to_display.plot(
    kind='bar',
    stacked=False
)

plt.show()