# Présentation
    
1. Est-il possible de prévoir la probabilité de subir un incident criminel selon le jour de la semaine et le district
2. Contexte    
  
Les données sont récupérer sur Kaggle.com

[Lien des données](https://www.kaggle.com/AnalyzeBoston/crimes-in-boston#offense_codes.csv)



# Collecte et exploitation des données
    . Compréhension de la données
    . Data visualisation (Shema)
    . Traitement et encodage 
# Modélisation 
    . Choix de l'algorithme
    . Validation et interprétaation
# Conclusion
    !! Apporter des réponses à la problématique