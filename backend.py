import numpy as np
import pandas as pd
import os

from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

import seaborn as sns


class MyRenderer:

    def __init__(self):
        self.name = '/datasource.xls'
        self.path = os.getcwd() + self.name
        self.dataset = pd.read_excel(self.path)
        self.dataset = self.dataset.rename(columns={
            "X": "Males",
            "Y": "Females",
        })
        self.X_train = np.expand_dims(self.dataset['Males'], axis=1)
        self.Y_train = self.dataset['Females']

        self.regr = linear_model.LinearRegression()
        self.regr_fit = self.regr.fit(self.X_train, self.Y_train, sample_weight=None)

        self.intercept = self.regr.intercept_
        self.coef = self.regr.coef_

        self.Y_pred = self.regr.predict(self.X_train)
        self.MSE = mean_squared_error(self.Y_train, self.Y_pred)
        self.score = r2_score(self.Y_train, self.Y_pred) * 100

    def get_api(self):
        return {
            'data': {
                'Name': self.name,
                'MSE': self.MSE,
                'Score': self.score
            }
        }